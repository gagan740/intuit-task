import {
    Button,
    Dialog,
    DialogActions,
    DialogTitle,
    Typography,
    Stack,
} from '@mui/material'

import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import DataPrivacyInitialContent from './DataPrivacyInitialContent'
import DataPrivacySelectionContent from './DataPrivacySelectionContent'

const DataPrivacyDialog = () => {
    const [selectionType, setSelectionType] = useState()

    const handleCancel = () => setSelectionType('')

    return (
        <Dialog onClose={null} open={true} fullWidth={true} maxWidth={'md'}>
            <DialogTitle>
                <Typography sx={{ fontWeight: 'bold', fontSize: '1.5rem' }}>
                    Data Privacy
                </Typography>
                <Typography variant='body2'>Manage your data</Typography>
            </DialogTitle>
            {!selectionType && (
                <DataPrivacyInitialContent
                    setSelectionType={setSelectionType}
                />
            )}
            {selectionType && <DataPrivacySelectionContent />}
            <DialogActions
                sx={{ justifyContent: 'space-between', padding: '20px 24px' }}
            >
                {!selectionType && (
                    <Typography data-testid='contact'>
                        If you have a privacy or security-related questions,{' '}
                        <Link to='#' style={{ textDecoration: 'none' }}>
                            contact us
                        </Link>
                    </Typography>
                )}
                {selectionType && (
                    <>
                        <Button variant='text' onClick={handleCancel}>
                            Cancel
                        </Button>
                        <Stack spacing={2} direction='row'>
                            <Button variant='outlined' onClick={handleCancel}>
                                Back
                            </Button>
                            <Button variant='contained' disabled>
                                Continue
                            </Button>
                        </Stack>
                    </>
                )}
            </DialogActions>
        </Dialog>
    )
}

export default DataPrivacyDialog
