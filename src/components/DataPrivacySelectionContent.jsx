import {
    Box,
    Button,
    Checkbox,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
} from '@mui/material'
import React, { useState } from 'react'
import { Link } from 'react-router-dom'

const rows = [
    {
        id: 1,
        name: 'Mint',
        text: "You'll get your transaction and bill payment histories, budgets, financial goals, credit report, any customized offers we provided, and your personal profile data",
        icon: '1',
        selectable: true,
    },
    {
        id: 2,
        name: 'Quick Books',
        text: 'Select companies that have personal data you want to download. This may include employee paychecks',
        icon: '2',
        selectable: false,
        url: true,
    },
    {
        id: 3,
        name: 'Family Heating and Air',
        text: 'QuickBooks Payments, QuickBooks Time',
        selectable: true,
    },
    {
        id: 4,
        name: 'Wisdom HVAC',
        text: 'Payroll, QuickBooks Time',
        selectable: true,
    },
    {
        id: 5,
        name: 'Turbo Tax',
        text: "You'll get tax return data, filing info, filing dates and return status as well as you order history and personal profile data.",
        icon: '3',
        selectable: true,
    },
    {
        id: 6,
        name: 'TurboTax Business',
        text: "Select the companies that have business tax return data you want to download. You'll get your company's tax return data, K-1 distributions, filingInfo, filing dates, and return status",
        icon: '4',
        selectable: false,
    },
    {
        id: 7,
        name: 'Family Heating and Air',
        text: 'QuickBooks Payments, QuickBooks Time',
        selectable: true,
    },
    {
        id: 8,
        name: 'Wisdom HVAC',
        text: 'Payroll, QuickBooks Time',
        selectable: true,
    },
]

const DataPrivacySelectionContent = () => {
    const [allChecked, setAllChecked] = useState(false)
    const [selectedRows, setSelectedRows] = useState([])

    const handleSelectAllClick = () => {
        if (!allChecked) {
            setAllChecked(true)
            const newSelectedIds = rows
                .filter((i) => i.selectable)
                .map((n) => n.id)
            setSelectedRows(newSelectedIds)
            return
        }
        setSelectedRows([])
        setAllChecked(false)
    }

    const isSelected = (id) => selectedRows.indexOf(id) !== -1

    const handleCheckboxSelection = (id) => {
        const selectedIndex = selectedRows.indexOf(id)
        let newSelected = []

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selectedRows, id)
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selectedRows.slice(1))
        } else if (selectedIndex === selectedRows.length - 1) {
            newSelected = newSelected.concat(selectedRows.slice(0, -1))
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selectedRows.slice(0, selectedIndex),
                selectedRows.slice(selectedIndex + 1)
            )
        }
        const isAllSelected =
            newSelected.length === rows.filter((i) => i.selectable).length
        setAllChecked(isAllSelected)
        setSelectedRows(newSelected)
    }

    return (
        <Box
            data-testid='selectionContent'
            sx={{
                padding: '20px 24px',
                borderTop: '1px solid rgba(0, 0, 0, 0.12)',
            }}
        >
            <Box
                sx={{
                    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
                    paddingBottom: '15px',
                }}
            >
                <Typography gutterBottom sx={{ fontWeight: 'bold' }}>
                    Select the data you want to download
                </Typography>
                <Typography gutterBottom>
                    We'll send you a link to download your data from the product
                    you select.
                </Typography>
            </Box>
            <Box>
                <TableContainer component={Paper} sx={{ border: 'none' }}>
                    <Table sx={{ minWidth: 650 }} aria-label='simple table'>
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ width: '80%' }}>
                                    <Typography
                                        gutterBottom
                                        sx={{ fontWeight: 'bold' }}
                                    >
                                        Your Intuit products
                                    </Typography>
                                </TableCell>

                                <TableCell sx={{ width: '20%' }} align='center'>
                                    <Button
                                        variant='text'
                                        onClick={handleSelectAllClick}
                                        role='checkboxSelection'
                                    >
                                        {!allChecked ? 'Select' : 'Unselect'}{' '}
                                        All
                                    </Button>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => {
                                const isItemSelected = isSelected(row.id)
                                return (
                                    <TableRow
                                        key={row.id}
                                        sx={{
                                            '&:last-child td, &:last-child th':
                                                {
                                                    border: 0,
                                                },
                                        }}
                                    >
                                        <TableCell
                                            sx={{
                                                display: 'flex',
                                            }}
                                        >
                                            <Box
                                                sx={{
                                                    width: '10px',
                                                    marginRight: '5px',
                                                }}
                                            >
                                                {row.icon}
                                            </Box>
                                            <Box>
                                                <Typography
                                                    gutterBottom
                                                    sx={{ fontWeight: 'bold' }}
                                                >
                                                    {row.name}
                                                </Typography>
                                                <Typography gutterBottom>
                                                    {row.text}{' '}
                                                    {row.url && (
                                                        <Link
                                                            to='#'
                                                            style={{
                                                                textDecoration:
                                                                    'none',
                                                            }}
                                                        >
                                                            learn more
                                                        </Link>
                                                    )}
                                                </Typography>
                                            </Box>
                                        </TableCell>

                                        <TableCell align='center'>
                                            {row.selectable && (
                                                <Checkbox
                                                    role='productCheckbox'
                                                    checked={isItemSelected}
                                                    onClick={() =>
                                                        handleCheckboxSelection(
                                                            row.id
                                                        )
                                                    }
                                                />
                                            )}
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    )
}

export default DataPrivacySelectionContent
