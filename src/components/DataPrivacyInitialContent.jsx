import { Box, DialogContent, IconButton, Typography } from '@mui/material'
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos'
import React from 'react'

const PRIVACY_OPTIONS = [
    {
        name: 'Download',
        text: 'Download a copy of all your personal data and a guide that helps explains what is includes.',
        type: 'download',
    },
    {
        name: 'Delete',
        text: 'Permanently delete all some of your personal data and learn how that will affect your accounts.',
        type: 'delete',
    },
    {
        name: 'Correction',
        text: 'Correct your personal data, by visiting the Personal Info section within your Intuit Account Settings or correct your data directly in product.',
        type: 'correction',
    },
]

const DataPrivacyInitialContent = ({ setSelectionType }) => {
    return (
        <Box data-testid='initialData'>
            <DialogContent dividers>
                <Typography gutterBottom sx={{ fontWeight: 'bold' }}>
                    We Promise protect your privacy
                </Typography>
                <Typography gutterBottom>
                    As technology advances. So do our privacy practices. We keep
                    our customers at the centers at the center of our innovation
                    as we create new ways to keep you and your data safe.
                </Typography>
            </DialogContent>
            {PRIVACY_OPTIONS.map((option) => (
                <DialogContent dividers key={option.type}>
                    <Box
                        sx={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            // flexDirection: { md: 'row', sm: 'column' },
                            // alignItems: { md: 'unset', sm: 'center' },
                        }}
                    >
                        <Typography
                            gutterBottom
                            sx={{
                                width: { md: '20%', sm: '30%' },
                                fontWeight: 'bold',
                            }}
                        >
                            {option.name}
                        </Typography>
                        <Typography
                            gutterBottom
                            sx={{ width: { md: '70%', sm: '60%' } }}
                        >
                            {option.text}
                        </Typography>
                        <IconButton
                            data-testid='nextButton'
                            aria-label='next'
                            sx={{ height: '10%' }}
                            onClick={() => setSelectionType(option.type)}
                        >
                            <ArrowForwardIosIcon />
                        </IconButton>
                    </Box>
                </DialogContent>
            ))}
        </Box>
    )
}

export default DataPrivacyInitialContent
