import {
    fireEvent,
    render,
    screen,
    waitFor,
    within,
} from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import DataPrivacyDialog from '../DataPrivacyDialog'

const DataPrivacyDialogWithBrowserRouter = (
    <BrowserRouter>
        <DataPrivacyDialog />
    </BrowserRouter>
)

describe('Data Privacy Dialog', () => {
    it('check if dialog displays with initial details', () => {
        render(DataPrivacyDialogWithBrowserRouter)

        const title = screen.getByText(/data privacy/i)
        const subTitle = screen.getByText(/manage your data/i)
        const initialContent = screen.getByTestId(/initialData/i)
        const contact = screen.getByTestId(/contact/i)

        expect(title).toBeInTheDocument()
        expect(subTitle).toBeInTheDocument()
        expect(initialContent).toBeInTheDocument()
        expect(contact).toBeInTheDocument()
    })

    describe('Data Privacy Dialog Grid', () => {
        it('should check if selection grid is displayed when button is clicked', async () => {
            render(DataPrivacyDialogWithBrowserRouter)
            const initialContent = await screen.findByTestId(/initialData/i)
            const nextButtons = await screen.findAllByTestId('nextButton')
            fireEvent.click(nextButtons[0])
            const selectionContent = await screen.findByTestId(
                /selectionContent/i
            )
            await waitFor(() => expect(selectionContent).toBeInTheDocument())
            await waitFor(() => expect(initialContent).not.toBeInTheDocument())
        })

        describe('Checkbox Select/Unselect All', () => {
            it('should check all Select all toggled', async () => {
                render(DataPrivacyDialogWithBrowserRouter)
                const nextButtons = await screen.findAllByTestId('nextButton')
                fireEvent.click(nextButtons[0])

                const selectAllButton = await screen.findByRole(
                    'checkboxSelection'
                )
                fireEvent.click(selectAllButton)
                const productCheckboxes = await screen.findAllByRole(
                    'productCheckbox'
                )

                expect(productCheckboxes.length).toEqual(
                    productCheckboxes.filter(
                        (i) => within(i).getByRole('checkbox').checked
                    ).length
                )
                fireEvent.click(selectAllButton)
                expect(productCheckboxes.length).not.toEqual(
                    productCheckboxes.filter(
                        (i) => within(i).getByRole('checkbox').checked
                    ).length
                )
            })
        })

        describe('Single Checkbox Selection', () => {
            it('should check checkbox toggled', async () => {
                render(DataPrivacyDialogWithBrowserRouter)
                const nextButtons = await screen.findAllByTestId('nextButton')
                fireEvent.click(nextButtons[0])
                const productCheckboxes = await screen.findAllByRole(
                    'productCheckbox'
                )
                const checkboxElement = within(productCheckboxes[0]).getByRole(
                    'checkbox'
                )
                fireEvent.click(checkboxElement)
                expect(checkboxElement.checked).toEqual(true)
                fireEvent.click(checkboxElement)
                expect(checkboxElement.checked).toEqual(false)
            })
            it('should check last checkbox toggled', async () => {
                render(DataPrivacyDialogWithBrowserRouter)
                const nextButtons = await screen.findAllByTestId('nextButton')
                fireEvent.click(nextButtons[0])
                const productCheckboxes = await screen.findAllByRole(
                    'productCheckbox'
                )
                const selectAllButton = await screen.findByRole(
                    'checkboxSelection'
                )
                fireEvent.click(selectAllButton)
                const checkboxElement = within(
                    productCheckboxes[productCheckboxes.length - 1]
                ).getByRole('checkbox')
                fireEvent.click(checkboxElement)
                expect(checkboxElement.checked).toEqual(false)
            })
            it('should check checkbox index > 0 toggled', async () => {
                render(DataPrivacyDialogWithBrowserRouter)
                const nextButtons = await screen.findAllByTestId('nextButton')
                fireEvent.click(nextButtons[0])
                const productCheckboxes = await screen.findAllByRole(
                    'productCheckbox'
                )
                const selectAllButton = await screen.findByRole(
                    'checkboxSelection'
                )
                fireEvent.click(selectAllButton)
                const checkboxElement = within(productCheckboxes[2]).getByRole(
                    'checkbox'
                )

                fireEvent.click(checkboxElement)
                expect(checkboxElement.checked).toEqual(false)
            })
        })
    })

    describe('Data Privacy Dialog handle cancel/back button', () => {
        it('should check if initial detail is displayed when back button is clicked', async () => {
            render(DataPrivacyDialogWithBrowserRouter)
            const nextButtons = await screen.findAllByTestId('nextButton')
            fireEvent.click(nextButtons[0])
            const backButton = screen.getByRole('button', { name: 'Back' })
            fireEvent.click(backButton)
            const initialContent = await screen.findByTestId(/initialData/i)
            await waitFor(() => expect(initialContent).toBeInTheDocument())
        })
        it('should check if initial detail is displayed when cancel button is clicked', async () => {
            render(DataPrivacyDialogWithBrowserRouter)
            const nextButtons = await screen.findAllByTestId('nextButton')
            fireEvent.click(nextButtons[0])
            const backButton = screen.getByRole('button', { name: 'Back' })
            fireEvent.click(backButton)
            const initialContent = await screen.findByTestId(/initialData/i)
            await waitFor(() => expect(initialContent).toBeInTheDocument())
        })
    })
})
