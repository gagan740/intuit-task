import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import DataPrivacyDialog from './components/DataPrivacyDialog'

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<DataPrivacyDialog />} />
            </Routes>
        </BrowserRouter>
    )
}

export default App
